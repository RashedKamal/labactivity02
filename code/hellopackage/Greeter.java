package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
// import java.util.*; to import all classes from the entire java.util package
public class Greeter {
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        //String str = s.nextLine();

        //java.util.Random rand = new java.util.Random();  
        //must be done if you don't import java.util.Random
        //Random r = new Random();

        System.out.println("Enter a number");
        int num = s.nextInt();
        System.out.println("Doubled value: " + Utilities.doubleMe(num));
    }
}